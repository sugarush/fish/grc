if type -q grc
  function ls -d "Colorize ls"
    grc ls $argv
  end

  function env -d 'Colorize env.'
    grc env $argv
  end

  function docker -d 'Colorize docker.'
    grc docker $argv
  end

  function docker-machine -d 'Colorize docker-machine.'
    grc docker-machine $argv
  end

  function du -d 'Colorize du.'
    grc du $argv
  end

  function df -d 'Colorize df.'
    grc df $argv
  end

  function whois -d 'Colorize whois.'
    grc whois $argv
  end

  function ping -d 'Colorize ping.'
    grc ping $argv
  end

  function traceroute -d 'Colorize traceroute.'
    grc traceroute $argv
  end

  function tcpdump -d 'Colorize tcpdump.'
    sudo tcpdump -l $argv | grcat "conf.tcpdump"
  end
end
